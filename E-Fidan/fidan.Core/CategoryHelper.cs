﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using fidan.Data;
namespace fidan.Core
{
    /// <summary>
    /// Category CRUID metodlarını içerir.
    /// </summary>
    public class CategoryHelper
    {
        private FidanConnection db = new FidanConnection();
        public List<Category> GetAllCategory()
        {
            List<Category> category = db.Categories.Where(w => w.Status == (byte)EnumCL.Status.Up).ToList();
            return category;
        }
        public List<Category> GetMainCategory()
        {

            var list = db.Categories.Where(w => w.SubCategoryId == (byte)EnumCL.SubCategory.None && w.Status==(int)EnumCL.Status.Up).ToList();
            return list;
        }
        public List<Category> GetSubCategories()
        {
            List<Category> list = db.Categories.Where(w => w.SubCategoryId != (byte)EnumCL.SubCategory.None).ToList();
            return list;
        }
        public CategoryModel GetById(int Id)
        {
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.Category = db.Categories.Where(w => w.Id == Id).FirstOrDefault();
            categoryModel.MainCategory = db.Categories.Where(w => w.Id == categoryModel.Category.SubCategoryId).FirstOrDefault();
            categoryModel.AllMainCategory = GetMainCategory();
            categoryModel.SubCategories = db.Categories.Where(w => w.SubCategoryId == Id && w.Status==(int)EnumCL.Status.Up).ToList();

            return categoryModel;
        }
        public void Create(Category model)
        {
            db.Categories.Add(model);
            db.SaveChanges();
        }
        public void Delete(Category model)
        {
            Category category = db.Categories.Where(w => w.Id == model.Id).FirstOrDefault();
            if (category != null)
            {
                category.Status = (byte)EnumCL.Status.Down;
            }
            db.SaveChanges();
        }
        public void Update(Category model)
        {
            Category category = db.Categories.Where(w => w.Id == model.Id).FirstOrDefault();
            category.Name = model.Name;
            db.SaveChanges();
        }
        public string GetCategoryName(int? Id)
        {
            string name = db.Categories.Where(w => w.Id == Id).Select(s=>s.Name).FirstOrDefault();
            return name;
        }
    }
    public class CategoryModel
    {
        public Category Category { get; set; }
        public Category MainCategory { get; set; }

        public List<Category> AllMainCategory { get; set; }
        public List<Category> SubCategories { get; set; }

    }
}