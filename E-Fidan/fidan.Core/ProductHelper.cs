﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using fidan.Data;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace fidan.Core
{


    public class ProductHelper
    {


        FidanConnection db = new FidanConnection();
        Product form = new Product();
        Guid guid = new Guid();
        Bitmap newImage;
        string path = HttpContext.Current.Server.MapPath("~/Upload/images/");
        /// <summary>
        /// statusu 1 olan tüm ürünleri getir
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProducts()
        {
            List<Product> productList = db.Products.Where(w => w.Status == (int)EnumCL.Status.Up).ToList();
            return productList;
        }
        /// <summary>
        /// yeni bir ürün oluşturma
        /// </summary>
        /// <param name="model"></param>
        public void Create(ProductModel model)
        {
            guid = Guid.NewGuid();
            form.FormId = guid;
            string path = HttpContext.Current.Server.MapPath("~/Upload/images/");
            string ext = Path.GetExtension(model.File.FileName).ToLower();
            var productPath = path;
            string[] validExt = { ".png", ".PNG", ".jpg", ".JPG", ".jpeg", ".JPEG" };
            if (validExt.Contains(ext))
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string fileName = string.Concat(guid.ToString(), Path.GetExtension(model.File.FileName));
                //model.File.SaveAs(Path.Combine(path, fileName));
                if (model.IsDisplayProduct)
                {
                    newImage = ResizeImage(Image.FromStream(model.File.InputStream, true, true), 900, 350);
                    newImage.Save(Path.Combine(path, "900px" + fileName), ImageFormat.Jpeg);
                    form.Status = (int)EnumCL.Status.DisplayProduct;//Display Product
                }
                newImage = ResizeImage(Image.FromStream(model.File.InputStream, true, true), 700, 400);
                newImage.Save(Path.Combine(path, fileName), ImageFormat.Jpeg);

            }
            form.Name = model.Name;
            form.Price = model.Price;
            form.Property = model.Property;
            form.Explanation = model.Explanation;
            form.CategoryId = model.CategoryId;
            db.Products.Add(form);
            db.SaveChanges();
        }
        /// <summary>
        /// FormId li ürünü getir
        /// </summary>
        /// <param name="FormId">unique resim adı</param>
        /// <returns></returns>
        public string GetReturnImage(Guid? FormId)
        {
            if (FormId != null)
            {
                //string port = "http://127.0.0.1:8887/";
                //string dir = HttpContext.Current.Server.MapPath(port);
                string path = Path.Combine(FormId + ".jpg");
                return path;
            }
            return "none";
        }
        /// <summary>
        /// seçilen ürünün silinmesi
        /// </summary>
        /// <param name="Id">ürün id</param>
        public void Delete(int Id)
        {
            Product dltProduct = db.Products.Where(w => w.Id == Id).FirstOrDefault();
            dltProduct.Status = (int)EnumCL.Status.Down;
            dltProduct.UpdatedDate = DateTime.Now;
            db.SaveChanges();
        }
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="model"></param>
        public void Update(ProductModel model)
        {
            Product editProduct = db.Products.Where(w => w.Id == model.Id).FirstOrDefault();
            if (model.File != null && model.File.ContentLength > 0)
            {

                guid = Guid.NewGuid();
                editProduct.FormId = guid;
                string ext = Path.GetExtension(model.File.FileName).ToLower();
                var productPath = path;
                string[] validExt = { ".png", ".PNG", ".jpg", ".JPG", ".jpeg", ".JPEG" };
                if (validExt.Contains(ext))
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    string fileName = string.Concat(guid.ToString(), Path.GetExtension(model.File.FileName));

                    newImage = ResizeImage(Image.FromStream(model.File.InputStream, true, true), 700, 400);
                    newImage.Save(Path.Combine(path, fileName));

                }
            }
            if (model.IsDisplayProduct)
            {
                string fileName = string.Concat(guid.ToString(), Path.GetExtension(model.File.FileName));
                newImage = ResizeImage(Image.FromStream(model.File.InputStream, true, true), 900, 350);
                newImage.Save(Path.Combine(path, "900px" + fileName ));
                editProduct.Status = (int)EnumCL.Status.DisplayProduct;//Display Product
            }
            editProduct.Name = model.Name;
            editProduct.CategoryId = model.CategoryId;
            editProduct.Explanation = model.Explanation;
            editProduct.Price = model.Price;
            editProduct.Property = model.Property;
            editProduct.UpdatedDate = DateTime.Now;
            db.SaveChanges();

        }
        public List<string> GetDisplayProduct()
        {
            List<string> image = new List<string>();
            var products = db.Products.Where(w => w.Status == (int)EnumCL.Status.DisplayProduct).Select(s => s.FormId).ToList();
            foreach (var item in products)
            {
                image.Add(Path.Combine("900px" + item + ".jpg"));
            }
            return image;
        }
        //public ProductViewModel GetAllProducts()
        //{
        //    ProductViewModel productView = new ProductViewModel();
        //    productView.Product = db.Products.Where(w => w.Status == (byte)EnumCL.Status.Up).ToList();
        //    return productView;
        //}
    }
    /// <summary>
    /// genişletilmiş ürün modeli
    /// </summary>
    public class ProductModel
    {
        public int Id { get; set; } // Id (Primary key)
        public string Name { get; set; } // Name (length: 100)
        public string Explanation { get; set; } // Explanation (length: 1000)
        public int? CategoryId { get; set; } // CategoryId
        public string Photo { get; set; } // Photo (length: 250)
        public string Price { get; set; } // Price (length: 50)
        public string Property { get; set; } // Property (length: 1000)
        public byte Status { get; set; } // Status
        public System.DateTime CreatedDate { get; set; } // CreatedDate
        public System.DateTime? UpdatedDate { get; set; } // UpdatedDate

        public HttpPostedFileBase File { get; set; }// Document
        public bool IsDisplayProduct { get; set; }//Teşhir Ürünü Mü?
    }
    public class ProductViewModel
    {
        public List<Product> Product { get; set; }
        public List<string> Image { get; set; }
    }
}