﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace fidan.Web.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        protected override void OnActionExecuting(ActionExecutingContext context)
        {
            if (Session["UserInformation"] == null)
            {
                context.Result = new RedirectResult("~/Home/Index");
                return;
            }

            base.OnActionExecuting(context);

            ViewBag.CurrentUserID = Session["UserId"];
        }
        public void CheckAuth()
        {
            if (Session["UserInformation"] == null)
            {
                Redirect("~/Home/Index");
            }
        }
    }
}