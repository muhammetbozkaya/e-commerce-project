﻿using fidan.Core.Classes;
using fidan.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace fidan.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        FidanConnection db = new FidanConnection();
        // GET: Login
        public JsonResult Login(string email, string password)
        {
            try
            {
                User user = db.Users.Where(w => w.Email == email && w.Password == password && w.AccountStatus == 1).FirstOrDefault();
                if (user == null)
                {
                    return Json(new { error = true, errorInformation = "Hatalı kullanıcı adı yada şifre" });
                }
                Session["userInformation"] = user;
                Session["userId"] = user.Id;
                Session["master"] = user.AccountState;
                Session["userName"] = $"{user.FirstName}{user.LastName}";
                Session["userMail"] = user.Email;
                user.IsOnline = true;
                db.SaveChanges();
                return Json(new { error = false, userName = Session["userName"] });

            }
            catch (Exception ex)
            {

                return Json(new { error = true, errorInformation = "beklenmedik bir hata oluştu." + ex.InnerException.Message });
            }
        }
        public ActionResult Logout()
        {
            try
            {
                Session.Abandon();
                Session["UserInformation"] = null;

                return Redirect("/Home/Index");
            }
            catch (Exception ex)
            {
                return Json(new { Error = true, ErrorInformation = "Bilinmeyen bir hata oluştu. Lütfen tekrar edneyiniz." + ex.InnerException.Message },JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(SignUpModel model)
        {
            User newUser = new User();
            newUser.FirstName = model.first_name;
            newUser.LastName = model.last_name;
            newUser.Email = model.email;
            newUser.Password = model.password;
            db.Users.Add(newUser);
            db.SaveChanges();
            return Redirect("/Home/Index");
        }
    }
}