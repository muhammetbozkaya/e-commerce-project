﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using fidan.Data;
using fidan.Core;

namespace fidan.Web.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        ProductHelper productHepler = new ProductHelper();
        CategoryHelper categoryHelper = new CategoryHelper();

        public ActionResult Index()
        {
            return View();
        }
        #region Products

        public ActionResult Products()
        {
            return View(productHepler.GetAllProducts());
        }
        public ActionResult CreateProducts()
        {
            CategoryModel model = new CategoryModel();
            model.AllMainCategory = categoryHelper.GetMainCategory();
            model.SubCategories = categoryHelper.GetSubCategories();
            return View(model);
        }
        public ActionResult AddProducts(ProductModel Model)
        {
            if (Model.File!=null && Model.File.ContentLength>0)
            {
                productHepler.Create(Model);
            }
            return Redirect("Products");
            //if (Request.Files.Count > 0)
            //{
            //    model.File = Request.Files[0];
            //    if (model.File !=null && model.File.ContentLength>0)
            //    {
            //        model.CategoryId = 1;
            //        productHepler.Create(model);
            //    }

            //}
            //return Redirect("CreateProduct");


            //if (model.CFile != null && model.CFile.ContentLength > 0)
            //{
            //    model.CategoryId = 1;
            //    productHepler.Create(model);
            //}
            //model.AllMainCategory = categoryHelper.GetMainCategory();
            //model.SubCategories=categoryHelper.GetSubCategories();
            //return View(model);
        }
        public ActionResult DeleteProducts(int Id)
        {
            productHepler.Delete(Id);
            return Redirect("products");
        }
        //[HttpPost]
        //public ActionResult AddProducts(Product model)
        //{
        //    model.CategoryId = 1;
        //    productHepler.Create(model);

        //    return Products();
        //}
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            return View();
        }
        [HttpPost]
        public ActionResult Edit(ProductModel model)
        {
            productHepler.Update(model);
            return Redirect("Products");
        }
        #endregion

        #region Category
        public ActionResult Category()
        {
            return View(categoryHelper.GetAllCategory());
        }
        [HttpGet]
        public ActionResult AddCategory()
        {
            List<Category> mainCategory = categoryHelper.GetMainCategory();
            List<SelectListItem> mainCategoryList = new List<SelectListItem>();
            foreach (var item in mainCategory)
            {
                mainCategoryList.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            return View(mainCategoryList);
        }
        [HttpPost]
        public ActionResult AddCategory(Category model)
        {
            
            if (model.SubCategoryId == null)
            {
                model.SubCategoryId = (byte)EnumCL.SubCategory.None;
            }
            categoryHelper.Create(model);
            return Category();
        }
        public ActionResult DeleteCategory(Category model)
        {
            categoryHelper.Delete(model);
            return Redirect("Category");
        }
        [HttpGet]
        public ActionResult UpdateCategory(int Id)
        {

            return View(categoryHelper.GetById(Id));
        }
        [HttpPost]
        public ActionResult UpdateCategory(Category model)
        {
            categoryHelper.Update(model);
            return Category();
        }
        #endregion
    }
}