﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using fidan.Core;
using fidan.Data;

namespace fidan.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        ProductHelper productHepler = new ProductHelper();
        CategoryHelper categoryHelper = new CategoryHelper();
        public int master = 2;
        public ActionResult Index()
        {
            return View(productHepler.GetAllProducts());
        }
        public JsonResult CheckLogin()
        {
            if (Session["UserInformation"] == null)
            {
                return Json(new { error = true, ErrorInformation = "Lütfen Giriş Yapınnız" },JsonRequestBehavior.AllowGet);
            }
            return Json(new { error = false },JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public  JsonResult CheckMaster()
        {
            int id = (int)Session["userId"];
            FidanConnection db = new FidanConnection();
            var user = db.Users.Where(w => w.Id == id).FirstOrDefault();
            if (user.AccountState==2)
            {
                return Json(new { error = false },JsonRequestBehavior.AllowGet);
            }
            return Json(new { error = true },JsonRequestBehavior.AllowGet);
        }
    }

}