﻿$(document).ready(function () {
    $("img").each(function () {
        var $img = $(this);
        $img.width($img.width() * .25);
    });
    $('.update').on('click', function (e) {
        e.preventDefault();
        var editableRow=$(this).closest('tr').html();
        bootbox.confirm({
            message:"<table><tbody><tr>"+editableRow+"</tr></tbody></table>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        });
    });
});