﻿$('.sepeteEkle').click(function (event) {
    event.preventDefault();
    //this.blur(); // Manually remove focus from clicked link.
    //$.get(this.href, function (html) {
    //    $(html).appendTo('body').modal();
    //});
    $.ajax({
        type: 'POST',
        url: "/Home/CheckLogin",
        success: function (response) {
            if (response.error == false) {
                bootbox.alert("sepete Eklendi");
            }
            else {
                
                bootbox
            .dialog({
                title: 'Login',
                message: $('#loginForm'),
                show: false 
            })
            .on('shown.bs.modal', function () {
                $('#loginForm')
                    .show();
            })
            .on('hide.bs.modal', function (e) {
                $('#loginForm').hide().appendTo('body');
            })
            .modal('show');
            }
        }
    })
    var loginForm = '<div id="sepeteEkle"><form action="" class="login_form modal" id="" style="display: inline-block;"><h3>Please login to continue</h3><p><label>Username:</label><input type="text"></p><p><label>Password:</label><input type="password"></p><p><input type="submit" value="Login"></p><a href="#close-modal" rel="modal:close" class="close-modal ">Close</a></form></div>';
    //bootbox.alert(loginForm);

    //$('#loginForm').formValidation({
    //    framework: 'bootstrap',
    //    icon: {
    //        valid: 'glyphicon glyphicon-ok',
    //        invalid: 'glyphicon glyphicon-remove',
    //        validating: 'glyphicon glyphicon-refresh'
    //    },
    //    fields: {
    //        username: {
    //            validators: {
    //                notEmpty: {
    //                    message: 'The username is required'
    //                }
    //            }
    //        },
    //        password: {
    //            validators: {
    //                notEmpty: {
    //                    message: 'The password is required'
    //                }
    //            }
    //        }
    //    }
    //});
    $('#Login').on('click', function (e) {
        var model = $('#loginForm').serialize();
        var Email = $('#EMail').val();
        var pass = $('#Password').val();
        $.ajax({
            type: 'POST',
            url: "/Account/Login",
            data: { email: Email, password: pass },
            success: function (response) {
                if (response.error==false) {
                    bootbox.hideAll();
                    $('#giris').remove();
                    $('#kayit').remove();
                    $('#Menu').append('<li class="nav-item" id="cikis"><a class="nav-link" href="/Account/Logout" >Çıkış</a></li>');
                    toastr.success(response.userName, "Hoş Geldiniz");
                    $.ajax({
                        url: "/Home/CheckMaster",
                        success: function (data) {
                            if(data.error==false){
                                $('#Menu').append('<li class="nav-item"><a class="nav-link" href="/Admin" >Admin Paneli</a></li>');
                            }

                            }
                        })
                    
                }
                else {
                    toastr.warning("Hatalı kullanıcı adı yada şifre","Hata")
                }
            }

        })
    });
});
//$(document).on('click', '.trigger', function (event) {
//    event.preventDefault();
//    // $('#modal').iziModal('setZindex', 99999);
//    // $('#modal').iziModal('open', { zindex: 99999 });
//    $('#modal').iziModal('open');
//});