﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinav.Classes
{
    public class ViewModel
    {
        public string MenuName { get; set; }
        public string MenuContent { get; set; }
        public int Id{ get; set; }
        public int Sira  { get; set; }
    }
    public class MenuContent
    {
        public string Content { get; set; }
    }
    public class SignUpModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
    public class kullanici
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Ceptel { get; set; }
        public string Evtel { get; set; }
        public string Sehir { get; set; }
        public string Aciklama { get; set; }
        public string Adres { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Id { get; set; }
    }
}