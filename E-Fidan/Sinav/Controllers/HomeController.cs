﻿using fidan.Data;
using Sinav.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sinav.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        FidanConnection db = new FidanConnection();
        Kullanici kullanici = new Kullanici();

        public ActionResult Index()
        {
            //List<ViewModel> viewList = new List<ViewModel>();
            //List<Menu> menus = db.Menus.OrderBy(o=>o.Id).ToList();
            //foreach (var item in menus)
            //{
            //    ViewModel model = new ViewModel();
            //    model.MenuName = item.Name;
            //    model.MenuContent = item.Content;
            //    model.Id = item.Id;
            //    viewList.Add(model);
            //}
            return View();

        }
        public ActionResult Menu(int id)
        {
            var a = db.Menus.Where(w => w.Id == id).FirstOrDefault();

            string content = a.Content;
            return Json(new { error=false ,data=content },JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult Login(string email,string password)
        {
            kullanici = db.Kullanicis.Where(w => w.Email == email && w.Password == password).FirstOrDefault();
            if (kullanici!=null)
            {
                return Redirect("/Home/Rehber");
            }
            return Redirect("/Home/Index");
        }
        public ActionResult Rehber()
        {
            var users = db.Kullanicis.Where(w => w.Status == true).ToList();
            return View(users);
        }
        public ActionResult Sil(int Id)
        {
            kullanici = db.Kullanicis.Where(w => w.Id == Id).FirstOrDefault();
            kullanici.Status = false;
            db.SaveChanges();
            return Redirect("/Home/Rehber");
        }
        public ActionResult Duzenle(int Id)
        {
            kullanici = db.Kullanicis.Where(w => w.Id == Id).FirstOrDefault();
            kullanici k = new Classes.kullanici();
            k.Adres = kullanici.Adres;
            k.Name = kullanici.Ad;
            k.Surname = kullanici.Soyad;
            k.Ceptel = kullanici.Ceptel;
            k.Evtel = kullanici.Evtel;
            k.Sehir = kullanici.Sehir;
            k.Aciklama = kullanici.Aciklama;
            k.Id = kullanici.Id;
            k.Email = kullanici.Email;
            k.Password = kullanici.Password;

            return View(k);
        }
        public ActionResult Duzenle2(kullanici k)
        {
            kullanici = db.Kullanicis.Where(w => w.Id == k.Id).FirstOrDefault();
            kullanici.Ad = k.Name;
            kullanici.Soyad = k.Surname;
            kullanici.Ceptel = k.Ceptel;
            kullanici.Evtel = k.Evtel;
            kullanici.Email = k.Email;
            kullanici.Password = k.Password;
            kullanici.Sehir = k.Sehir;
            kullanici.Adres = k.Adres;
            kullanici.Aciklama = k.Aciklama;
            db.SaveChanges();
            return Redirect("/Home/Rehber");
        }
        public ActionResult Arsiv()
        {
            var kullanici = db.Kullanicis.Where(w => w.Status == false).ToList();
            
           
            return View(kullanici);
        }
    }
}